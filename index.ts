import * as pulumi from "@pulumi/pulumi";

const path = '%2F';
// const path = '%2f';
const path1 = pulumi.output(path);
export const path2 = path1.apply(path=>`${path}`);
