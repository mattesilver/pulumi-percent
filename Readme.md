1. run `pulumi up` to initialize stack and create an output
2. in index.ts change path to `%2f` (from upper case to lower)
3. run `pulumi up` again
    should show output change:

```console
Outputs:
  ~ path2: "%!(NOVERB)F" => "%!(NOVERB)f"
```
